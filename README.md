# ANOIAcam

## WHAT IS THIS?
ANOIAcam is a project in development for the construction of a Raspberry Pi based timelapse camera for river monitoring.

Te camera takes one picture every hour.


## BEFORE START
Download latest Raspbian operating sistem and flash it using [Etcher](https://etcher.io).

Create a blank text document and save it at the boot partition of the SD card with the name "ssh" to enable remote connection with the Pi.

Download the "wpa_supplicant.conf" document and set your wifi credentials.

Save it at the boot partition of the SD card. The Raspberry Pi will automatically paste it wher needed.

After that the Raspberry Pi should connect automatically to your wifi network.

Connect your raspberry pi to a monitor or connect using SSH.