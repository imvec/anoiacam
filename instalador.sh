# !/bin/bash
#
# CÓMO UTILIZAR ESTE SCRIPT DE INSTALACIÓN
# Instala la última versión de RaspberryPiOS Lite y flashéala usando Etcher.io.
# Enchufa la Raspberry a un monitor y un teclado.
# Enciéndela.
# Entra en la carpeta de usuaria escribiendo "cd /home/pi"
# Descarga este archivo con "sudo wget https://gitlab.com/imvec/anoiacam/raw/master/instalador.sh"
# Concédele permiso de ejecución con "sudo chmod +x installer.sh"
# Ejecuta el archivo con "sudo ./installer.sh"
#
#
# HOW TO USE THIS INSTALLATION SCRIPT
# Install latest version of Raspbian Lite and flash it using Etcher.io
# Plug the Raspberry to a monitor and a keyboard. 
# Boot it up.
# Go to your user directory typing "cd /home/pi"
# Get this installer file with "sudo wget https://gitlab.com/imvec/anoiacam/raw/master/instalador.sh"
# Give execute permission to the file with "sudo chmod +x installer.sh"
# Execute the file with "sudo ./installer.sh"
#
#
 echo ""
 echo ""
 echo ""
 echo "=============================="
 echo "   Preparando la instalación  "
 echo "=============================="
 echo ""  
sleep 2
 echo ""  
 echo "==== Actualizando apt-get ===="
 echo ""
 echo "Actualización de los repositorios de programas por si tu versión del sistema operativo está desactualizada"
 echo ""
 echo ""
sleep 3
 echo ""
apt-get update
 echo ""
 echo "==== Activando cámara y conexión i2C ===="
 echo ""
 echo "Se descarga un nuevo archivo de configuración de raspberry para sua activación automática"
 echo ""
 echo ""
sleep 3
 echo ""
cd /boot
rm -rf config.txt
wget https://gitlab.com/imvec/anoiacam/raw/master/config.txt
 echo ""
 echo "==== Creando carpetas ===="
 echo ""
 echo "Se crea la carpera ANOIAcam en la ruta /home/pi y en su interior dos carpetas más: media y scripts"
 echo ""
 echo ""
sleep 3
 echo ""
mkdir /home/pi/ANOIAcam
mkdir /home/pi/ANOIAcam/media
mkdir /home/pi/ANOIAcam/scripts
 echo ""
 echo "==== Adquiriendo el script de cámara ===="
 echo ""
 echo "Se descarga el script que hará que la ANOIAcam se dispare cada vez que arranque y se apague despues de 5 minutos"
 echo ""
 echo ""
sleep 3
 echo ""
cd /home/pi/ANOIAcam/scripts
wget https://gitlab.com/imvec/anoiacam/raw/master/anoiacam.sh
chmod +x anoiacam.sh
 echo ""
 echo "==== Adquiriendo instalador de Witty Pi ===="
 echo ""
 echo "Se descarga el instalador para manejar el control de encendido y apagado de la cámara cada hora"
 echo ""
 echo ""
sleep 3 
 echo ""
cd /home/pi
wget https://gitlab.com/imvec/anoiacam/-/raw/master/installWittyPi.sh
chmod +x installWittyPi.sh
 echo ""
 echo "=================================================================="
 echo "====            Instalando el software de Witty Pi            ===="
 echo "=================================================================="
 echo "====                         Responde:                        ===="
 echo "==== Remove fake-hwclock package and disable ntpd daemon? → Y ===="
 echo "==== Do you want to install Qt 5 for GUI running? → N         ===="
 echo "=================================================================="
 echo ""
 echo ""
sleep 10
 echo ""
./installWittyPi.sh
 echo ""
 echo "==== Adquiriendo archivo de calendarización de la cámara ===="
 echo ""
 echo "Este archivo se ubicará en la ruta /home/pi/wittyPi/schedules/anoiacam.wpi"
 echo ""
 echo ""
sleep 3
 echo ""
 cd /home/pi/wittyPi/schedules
 wget https://gitlab.com/imvec/anoiacam/-/raw/master/anoiacam.wpi
 echo ""
 echo "==== Actualizando apt-get ===="
 echo ""
 echo ""
sleep 2
 echo ""
apt-get update
 echo ""
 echo "==== Instalando Motion ===="
 echo ""
 echo "Motion permitirá ver el encuadre de cámara usando un navegador y visitando http://IPdetucamara:8081"
 echo ""
 echo ""
sleep 3
 echo ""
apt-get install motion -y --fix-missing
 echo ""
 echo "==== Adquiriendo archivos de configuración de Motion ===="
 echo ""
 echo "El archivo de configuración se ubicará en la ruta /etc/motion/motion.conf"
 echo ""
 echo ""
sleep 3
 echo ""
cd /etc/motion
rm -rf motion.conf
wget https://gitlab.com/imvec/anoiacam/raw/master/motion.conf
cd /etc/default
rm -rf motion
wget https://gitlab.com/imvec/anoiacam/raw/master/motion
 echo ""    
 echo "==== Configurando Motion para activarse tras cada reinicio ===="
 echo ""
 echo ""
sleep 3
 echo ""
systemctl enable motion
 echo ""
 echo "==== Adquiriendo script de respaldo con Rsync ===="
 echo ""
 echo "En principio no lo usarás pero permite (una vez configurado) respaldar las imágenes obtenidas en un servidor remoto"
 echo ""
 echo ""
sleep 3
 echo ""
cd /home/pi/ANOIAcam/scripts
wget https://gitlab.com/imvec/anoiacam/-/raw/master/rsync.sh
chmod +x rsync.sh
 echo ""
 echo "==== Eliminando instaladores ===="
 echo ""
 echo "Sin comentario ;D"
 echo ""
 echo ""
sleep 3
 echo ""
cd /home/pi
rm -rf instalador.sh
rm -rf installWittyPi.sh
  echo ""
  echo ""
  echo ""
  echo "========================================================================"
  echo "=                           SIGUIENTES PASOS                           ="
  echo "========================================================================"
  echo "=   Apaga la cámara, enchufa placa la Witty Pi y reinicia la cámara    ="
  echo "========================================================================"
  echo "=    Debes configurar la hora de tu Raspberry con sudo raspi-config    ="
  echo "=     Configura la placa Witty Pi con: sudo ./wittyPi/wittyPi.sh       ="
  echo "========================================================================"
  echo "= Escribe: sudo crontab -e y añade estas lineas al final del archivo   ="
  echo "= para que la cámara dispare una fotografia al iniciar la Raspberry y  ="
  echo "= se apague a los cinco minutos:                                       ="
  echo "= @reboot sudo sh /home/pi/ANOIAcam/scripts/anoiacam.sh                ="             
  echo "========================================================================"
  echo "=  Si quieres respaldo remoto de las imágenes debes editar el archivo  ="
  echo "=  rsync.sh y editar de nuevo el crontab que queda así:                ="
  echo "=     @reboot sudo sh /home/pi/ANOIAcam/scripts/anoiacam.sh            ="
  echo "=     @reboot sudo sh /home/pi/ANOIAcam/scripts/rsync.sh               ="
  echo "========================================================================"
  echo ""
  echo ""
sleep 3
  echo ""
  echo ""
  echo "========================================================================"
  echo "=              ¿Dudas o problemas durante la instalación?              ="
  echo "=         Ponte en contacto con nosotras en imvec@tutanota.com         ="
  echo "========================================================================"
  echo ""
  echo ""
  echo ""
  echo ""
