#!/bin/bash

DATE=$(date +"%Y-%m-%d_%H:%M")
DATE=$"$DATE.png"
raspistill -n -w 2592 -h 1944 -q 100 -rot 180 -o /home/pi/ANOIAcam/media/$DATE
sleep 300
poweroff
